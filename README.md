# SkillsTestMay2021

General Instructions:
- Develop a simple Library System that implements the following specifications:

> **BACKEND** (preloaded database: Library.sql / Library.mdb / Library.accdb)
- A libary database with the following tables each with its corresponding fields:

| Authors Table          | Books Table            | Students Table         | BorrowedBooks Tables     |
| ---------------------- | ---------------------- | ---------------------- | ------------------------ |
| **authID (number) PK** | **bookID (number) PK** | **studID (number) PK** | **borrowID (number) PK** |
| authLName (text)       | bookTitle (text)       | studFName (text)       | studID (number) FK       |
| authFName (text)       | bookCopies (number)    | studMName (text)       | bookID (number) FK       |
| authMName (text)       | authID (number) FK     | studLName (text)       | status (text)            |


> **FRONTEND**
- A library application with the following modules each with its corresponding functionalities:


| MODULES | WEIGHT | SCORE |
| ------- | ------ | ----- |
| - _Provide for an Authors Management **UI** that allows:_ <br> 1. Adding & updating an author record **(0.5pts)** <br> 2. Searching an author record **(0.5pts)** <br> 3. Deleting an author record **(0.5pts)**<br> 4. Viewing of author records **(0.5pts)** | **2** | |
| - _Provide for a Books Management **UI** that allows:_ <br> 1. Adding & updating a book record **(0.5pts)** <br> 2. Searching a book record **(0.5pts)** <br> 3. Deleting a book record **(0.5pts)** <br> 4. Viewing of student records **(0.5pts)**| **2** | |
| - _Provide for a Students Managment **UI** that allows:_ <br> 1. Adding & updating a student record **(0.5pts)** <br> 2. Searching a student record **(0.5pts)** <br> 3. Deleting a student record **(0.5pts)** <br> 4. Viewing of student records **(0.5pts)**| **2** | |
| - _Provide for a Book Borrowing **UI** that allows:_ <br> 1. Inquiring availability of book record (as against bookCopies **(1pt)**) <br> 2. Borrowing of books **(2pts)** <br> 3. Viewing of only borrowed (not yet returned) book records **(1pt)** <br> 4. Returning of borrowed books **(2pts)** <br> 5. Viewing of only returned book records **(2pts)** | **8** | |
| - **Q & A** <br> Ability to answer 3 questions| **6** | |
| **TOTAL SCORE** | **20** | |
| **CHECKED BY:** |


> GitLab Instructions

- Clone the repo
    - `git clone https://gitlab.com/redwin1823/skillstestmay2021.git` 
- create a new branch using your name separated by an underscore(_). _This is very important, don't forget to create before you start coding._
    - `git checkout -b maria_juana_dela_cruz`
- Start working on your code.
    - For each file, add your name as an author on the first line as a comment.
        - `// Author: Maria Juana Dela Cruz`
- After you've created your files, add them to your branch.
    - `git add .`
- Commit all your changes
    - `git commit -am "Your message here"`
- Push your changes to your branch.
    - `git push origin maria_juana_dela_cruz`
- You are all set after pushing it to your branch. 
